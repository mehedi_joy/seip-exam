<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Status::Create(['name' => 'processing']);
        Status::Create(['name' => 'shipped']);
        Status::Create(['name' => 'delivered']);
        Status::Create(['name' => 'cancelled']);
    }
}
