<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Product::create([
            'name' => 'Shirts'
        ]);

        Product::create([
            'name' => 'Pants'
        ]);

        Product::create([
            'name' => 'Shoes'
        ]);
    }
}
