<?php

namespace Database\Seeders;

use App\Models\Order;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Order::create([
            'product_id' => 1,
            'unit_price' => 56.14,
            'quantity' => 12,
            'unit_id' => 1,
            'delivery_date' => '2016-12-31',
            'status_id' => 1
        ]);

        Order::create([
            'product_id' => 2,
            'unit_price' => 78.45,
            'quantity' => 10,
            'unit_id' => 1,
            'delivery_date' => '2016-10-11',
            'status_id' => 1
        ]);

        Order::create([
            'product_id' => 3,
            'unit_price' => 12.87,
            'quantity' => 8,
            'unit_id' => 1,
            'delivery_date' => '2016-09-12',
            'status_id' => 1
        ]);
    }
}
