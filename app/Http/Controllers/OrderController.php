<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Models\Order;
use App\Models\Product;
use App\Models\Status;
use App\Models\Unit;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index() {
        $data = Order::all();
        return view('components.order.orderShow', compact('data'));
    }

    public function show($id) {
        $data = Order::find($id);
        return view('components.order.orderShowOne', compact('data'));
    }

    public function create() {
        $products = Product::all();
        $units = Unit::all();
        $statuses = Status::all();

        return view('components.order.orderCreate', [
            'products' => $products,
            'units' => $units,
            'statuses' => $statuses
        ]);
    }

    public function store(OrderRequest $request) {
        $requestData = [
            'product_id' => $request->input('product_id'),
            'unit_price' => $request->input('unit_price'),
            'quantity' => $request->input('quantity'),
            'unit_id' => $request->input('unit_id'),
            'delivery_date' => $request->input('delivery_date'),
            'status_id' => $request->input('status_id')
        ];

        Order::create($requestData);

        return redirect()->route('Order.Index')->with('message', 'Successfully Saved!');
    }

    public function edit($id) {
        $data = Order::find($id);
        $products = Product::all();
        $units = Unit::all();
        $statuses = Status::all();

        return view('components.order.orderEdit', [
            'products' => $products,
            'units' => $units,
            'statuses' => $statuses,
            'data' => $data
        ]);
    }

    public function update($id, OrderRequest $request) {
        $requestData = [
            'product_id' => $request->input('product_id'),
            'unit_price' => $request->input('unit_price'),
            'quantity' => $request->input('quantity'),
            'unit_id' => $request->input('unit_id'),
            'delivery_date' => $request->input('delivery_date'),
            'status_id' => $request->input('status_id')
        ];

        $data = Order::find($id);
        $data->update($requestData);

        return redirect()->route('Order.Index')->with('message', 'Updated Successfully!');
    }

    public function delete($id) {
        $data = Order::find($id);
        $data->delete();

        return redirect()->route('Order.Index')->with('message', 'Entry Deleted Successfully!');
    }
}
