<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index() {
        $data = Product::all();
        return view('components.product.productShow', compact('data'));
    }

    public function show($id) {
        $data = Product::find($id);
        return view('components.product.productShowSingle', compact('data'));
    }

    public function create() {
        return view('components.product.productCreate');
    }

    public function store(ProductRequest $request) {
        $requestData = [
            'name' => $request->input('name'),
            'stocks' => $request->input('stocks'),
            'trending' => $request->input('trending') ? 1 : 0
        ];


        Product::create($requestData);

        return redirect()->route('Product.Index')->with('message', 'Product Entry Successfull!');
    }

    public function delete($id) {
        $reqeustData = Product::find($id);
        $reqeustData->delete();

        return redirect()->route('Product.Index')->with('message', 'Product Deletion Successfull!');
    }

    public function trash() {
        $data = Product::onlyTrashed()->get();
        return view('components.product.productTrashShow', compact('data'));
    }
}
