<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'product_id' => 'bail|required|numeric',
            'unit_price' => 'bail|required|numeric',
            'quantity' => 'bail|required|numeric',
            'unit_id' => 'bail|required|numeric',
            'delivery_date' => 'bail|required|date',
            'status_id' => 'bail|required|numeric'
        ];
    }
}
