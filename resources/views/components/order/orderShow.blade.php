<x-master>
    <x-order.orderNavigation />
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Product</th>
                <th>Unit Price</th>
                <th>Quantity</th>
                <th>Units</th>
                <th>Delivery Date</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ( $data as $order )
                <tr>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->product->name }}</td>
                    <td>{{ $order->unit_price }}</td>
                    <td>{{ $order->quantity }}</td>
                    <td>{{ $order->unit->name }}</td>
                    <td>{{ $order->delivery_date }}</td>
                    <td>{{ $order->status->name }}</td>
                    <td>
                        <a href="{{ route('Order.Show', $order->id) }}" class="btn btn-outline-info">Show</a>
                        <a href="{{ route('Order.Edit', $order->id) }}" class="btn btn-outline-warning">Edit</a>
                        <form action="{{ route('Order.Delete', $order->id) }}" method="post" class="d-inline">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-outline-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</x-master>
