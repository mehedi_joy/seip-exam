<x-master>
    <x-order.orderNavigation />
    <div class="showData" style="width: 30vw;margin: auto;">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form action="{{ route('Order.Update', $data->id) }}" method="post">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="product_name" class="form-label">Product Name</label>
                <select class="form-control" name="product_id" id="product_name" required>
                    @foreach ($products as $product)
                    <option value="{{ $product->id }}" @if ($product->id == $data->product_id) selected @endif>{{ $product->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="unit_price" class="form-label">Unit Price</label>
                <input class="form-control" type="number" name="unit_price" id="unit_price" value="{{ $data->unit_price }}" required>
            </div>
            <div class="form-group">
                <label for="quantity" class="form-label">Quantity</label>
                <input class="form-control" type="number" name="quantity" id="quantity" value="{{ $data->quantity }}" required>
            </div>
            <div class="form-group">
                <label for="unit" class="form-label">Units</label>
                <select class="form-control" name="unit_id" id="unit" required>
                    @foreach ($units as $unit)
                    <option
                        value="{{ $unit->id }}"
                        @if ($unit->id == $data->unit_id)
                        selected
                        @endif>
                        {{ $unit->name }}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="delivery_date" class="form-label">Delivery Date</label>
                <input class="form-control" type="date" name="delivery_date" id="delivery_date" value="{{ $data->delivery_date }}" required>
            </div>
            <div class="form-group">
                <label for="status" class="form-label">Status</label>
                <select class="form-control" name="status_id" id="status" required>
                    @foreach ($statuses as $status)
                    <option value="{{ $status->id }}" @if ($status->id == $data->status_id) selected @endif>{{ $status->name }}</option>
                    @endforeach
                </select>
            </div>
            <input class="btn btn-outline-primary" type="submit" value="Update">
        </form>
    </div>
</x-master>
