<x-master>
    <x-order.orderNavigation />
    <div class="showData" style="width: 30vw;margin: auto;">
        <div class="form-group">
            <label for="order_id" class="form-label">ID</label>
            <input class="form-control" type="text" name="order_id" id="order_id" value="{{ $data->id }}" disabled>
        </div>
        <div class="form-group">
            <label for="product_name" class="form-label">Product Name</label>
            <input class="form-control" type="text" name="product_name" id="product_name"value="{{ $data->product->name }}" disabled>
        </div>
        <div class="form-group">
            <label for="unit_price" class="form-label">Unit Price</label>
            <input class="form-control" type="number" name="unit_price" id="unit_price"value="{{ $data->unit_price }}" disabled>
        </div>
        <div class="form-group">
            <label for="quantity" class="form-label">Quantity</label>
            <input class="form-control" type="number" name="quantity" id="quantity"value="{{ $data->quantity }}" disabled>
        </div>
        <div class="form-group">
            <label for="unit" class="form-label">Units</label>
            <input class="form-control" type="text" name="unit" id="unit"value="{{ $data->unit->name }}" disabled>
        </div>
        <div class="form-group">
            <label for="delivery_date" class="form-label">Delivery Date</label>
            <input class="form-control" type="date" name="delivery_date" id="delivery_date"value="{{ $data->delivery_date }}" disabled>
        </div>
        <div class="form-group">
            <label for="status" class="form-label">Status</label>
            <input class="form-control" type="text" name="status" id="status" value="{{ $data->status->name }}" disabled>
        </div>
    </div>
</x-master>
