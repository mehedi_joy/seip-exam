<div class="row mb-3 mt-3">
    <a class="btn btn-primary mr-3" href="{{ route('Order.Index') }}">
        <span data-feather="menu"></span>
        Index
    </a>
    <a class="btn btn-outline-primary" href="{{ route('Order.Create') }}">
        <span data-feather="plus"></span>
        Create
    </a>
</div>
