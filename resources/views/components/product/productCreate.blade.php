<x-master>
    <div class="card" style="width: 60vw;margin: auto;">
        <div class="card-body">
            <form action="{{ route('Product.Store') }}" method="post">
                @csrf
                @method('post')
                <x-form.inputForm id="product_name" name="name" />
                <x-form.inputForm id="product_stocks" name="stocks" type="number" step="any"/>
                <x-form.radioForm id="product_trending" name="trending"/>
                <x-form.submitForm type="primary" value="Save" />
            </form>
        </div>
    </div>
</x-master>
