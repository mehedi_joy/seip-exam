<x-master>
    <x-product.productNavigation/>
    <div class="card" style="width: 60vw;margin: auto;">
        <div class="card-body">
            <x-form.inputForm name="product name" id="product_name" type="text" :data="$data->name" disabled/>
            <x-form.inputForm name="product stocks" id="product_stocks" type="number" step="6" :data="$data->stocks" disabled/>
            <x-form.radioForm name="is trending" id="is_trending" :data="$data->trending"/>
        </div>
    </div>
</x-master>
