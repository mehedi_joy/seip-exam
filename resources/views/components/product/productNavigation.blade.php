<div class="row m-3">
    <a href="{{ route('Product.Index') }}" class="btn btn-sm btn-primary">
        <i data-feather="list" height="20px" width="20px"></i>
        Index
    </a>
    <div class="more-options">
        <a href="{{ route('Product.Create') }}" class="btn btn-sm btn-outline-primary">
            <span data-feather="plus" height="20px" width="20px"></span>
            Add
        </a>
        <a href="{{ route('Product.Trash') }}" class="btn btn-sm btn-outline-primary">
            <span data-feather="trash" height="20px" width="20px"></span>
            Trash
        </a>
    </div>
</div>
