<x-master>
    <x-product.productNavigation/>
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Stocks</th>
                <th>Is Trending?</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $product)
                <tr>
                    <td>{{ $product->id }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->stocks }}</td>
                    <td>
                        @if ($product->trending)
                            Yes
                        @else
                            No
                        @endif
                    </td>
                    <td>
                        <a href="{{ route('Product.Show', $product->id) }}" class="btn btn-sm btn-outline-info">Show</a>
                        <form action="{{ route('Product.Delete', $product->id) }}" method="POST" class="d-inline">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-sm btn-outline-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</x-master>
