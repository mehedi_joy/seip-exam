@props(['name', 'id', 'data' => '',])


<div class="form-group">
    <label for="{{ $id }}" class="form-label">{{ ucwords($name) }}</label>
    <input id="{{ $id }}" name="{{ $name }}" class="form-control" {{ $attributes }} value="{{ $data }}">
</div>
