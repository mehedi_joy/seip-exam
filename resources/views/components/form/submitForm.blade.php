@props(['value', 'type'])

<input type="submit" {{ $attributes->merge(['class' => 'btn btn-'.$type]) }} value="{{ $value }}"/>
