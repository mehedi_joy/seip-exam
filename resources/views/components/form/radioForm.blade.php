@props(['name', 'id', 'data' => 0,])

<div class="form-check">
    <input class="form-check-input" type="checkbox" value="" id={{ $id }} {{ $attributes }}
    @if ($data == 1)
        checked
    @endif
    >
    <label class="form-check-label" for={{ $id }}> {{ ucwords($name) }}</label>
</div>
