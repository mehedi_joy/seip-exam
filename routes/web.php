<?php

use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('Welcome');

Route::get('/orders', [OrderController::class, 'index'])->name('Order.Index');
Route::post('/orders', [OrderController::class, 'store'])->name('Order.Store');
Route::get('/orders/create', [OrderController::class, 'create'])->name('Order.Create');

Route::get('/orders/{id}', [OrderController::class, 'show'])->name('Order.Show');
Route::get('/orders/{id}/edit', [OrderController::class, 'edit'])->name('Order.Edit');
Route::put('/orders/{id}', [OrderController::class, 'update'])->name('Order.Update');
Route::delete('/orders/{id}', [OrderController::class, 'delete'])->name('Order.Delete');

Route::prefix('products')->group(function () {
    Route::get('/', [ProductController::class, 'index'])->name('Product.Index');
    Route::get('/create', [ProductController::class, 'create'])->name('Product.Create');
    Route::post('/create', [ProductController::class, 'store'])->name('Product.Store');
    Route::get('/trash', [ProductController::class, 'trash'])->name('Product.Trash');

    Route::get('/{id}', [ProductController::class, 'show'])->name('Product.Show');
    Route::delete('/{id}', [ProductController::class, 'delete'])->name('Product.Delete');
});
